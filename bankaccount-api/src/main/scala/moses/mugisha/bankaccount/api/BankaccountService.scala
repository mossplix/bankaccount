package moses.mugisha.bankaccount.api

import akka.{Done, NotUsed}
import com.lightbend.lagom.scaladsl.api.Service._
import com.lightbend.lagom.scaladsl.api.broker.Topic
import com.lightbend.lagom.scaladsl.api.broker.kafka.{KafkaProperties, PartitionKeyStrategy}
import com.lightbend.lagom.scaladsl.api.{Service, ServiceCall}
import play.api.libs.json.{Format, JsString, Json}
import com.lightbend.lagom.scaladsl.api.transport.Method
import org.joda.time.DateTime

import scala.collection.immutable.Seq

object BankaccountService  {
  val TOPIC_NAME = "greetings"
}

/**
  * The Bankaccount service interface.
  * <p>
  * This describes everything that Lagom needs to know about how to serve and
  * consume the BankaccountService.
  */
trait BankaccountService extends Service {

  /**
    * Example: curl http://localhost:9000/api/balance/Alice
    */
  def balance(id: String): ServiceCall[NotUsed, AccountStatus]

  /**
    * Example: curl -H "Content-Type: application/json" -X POST -d '{"message":
    * "Hi"}' http://localhost:9000/api/balance/Alice
    */
  def debit(id: String): ServiceCall[TransactionItem, Done]


  def credit(id: String): ServiceCall[TransactionItem, Done]


  /**
    * This gets published to Kafka.
    */
  def transactionTopic(): Topic[TransactionItem]

  override final def descriptor = {
    import Service._
    // @formatter:off
    named("Bankaccount")
      .withCalls(
        pathCall("/api/account/:id/balance", balance _),
        restCall(Method.POST,"/api/account/:id/debit", debit _),
          restCall(Method.POST,"/api/account/:id/credit", credit _)
      )
      .withTopics(
        topic(BankaccountService.TOPIC_NAME, transactionTopic _)
          // Kafka partitions messages, messages within the same partition will
          // be delivered in order, to ensure that all messages for the same user
          // go to the same partition (and hence are delivered in order with respect
          // to that user), we configure a partition key strategy that extracts the
          // name as the partition key.
          .addProperty(
          KafkaProperties.partitionKeyStrategy,
          PartitionKeyStrategy[TransactionItem](_.acc)
        )
      )
      .withAutoAcl(true)
    // @formatter:on
  }
}



/**
  * The greeting message class used by the topic stream.
  * Different than [[Transaction]], this message includes the name (id).
  */
case class TransactionItem(acc: String, amount: BigDecimal, credit: Boolean,date: DateTime = DateTime.now())

object  TransactionItem{
  implicit val format: Format[TransactionItem] = Json.format[TransactionItem]

}


case class AccountStatus(acc:String,balance: BigDecimal = 0,no_withdrawals_today: Int = 0,no_deposits_today: Int = 0,deposits_today: BigDecimal = 0,withdrawals_today: BigDecimal=0,date: String = DateTime.now().toLocalDate().toDate().toString)


object  AccountStatus{
  implicit val format: Format[AccountStatus] = Json.format[AccountStatus]



}


