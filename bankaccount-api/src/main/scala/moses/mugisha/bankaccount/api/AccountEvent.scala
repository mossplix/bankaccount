package moses.mugisha.bankaccount.api
import java.util.UUID

import org.joda.time.DateTime

/**
  * Created by mossplix on 7/3/17.
  *
  *
  *
  */

/**
  * A bid event.
  */
sealed trait Event {
  val id: UUID
  val at: DateTime
}



import common._
