package moses.mugisha.bankaccount.api

import com.fasterxml.jackson.annotation.JsonFormat
import com.lightbend.lagom.scaladsl.api.deser.PathParamSerializer
import org.joda.time.DateTime
import org.joda.time.DateTime
import play.api.libs.json.{Format, JsString, JsValue, Json}

//import play.api.libs.json._
//import spray.json._


/**
  * Created by mossplix on 7/3/17.
  */

object common {
  type Amount = BigDecimal
  type Error = String
  val today = DateTime.now()
  val MaxDailyDeposit = 150000
  val MaxDepositPerTransaction = 40000
  val MaxDailyDepositFrequency = 4
  val MaxDailyWithdrawal = 50000
  val MaxPerTransactionDeposit = 40000
  val MaxPerTransactionWithdrawal = 20000
  val MaxDailyWithdrawalFrequency = 3
}

import common._




sealed trait TransactionType
case object Debit extends TransactionType
case object Credit extends TransactionType

case class Day(year: Int, month: Int,day: Int,hour: Int)


object TransactionType {
  def apply(s: String) = s.toLowerCase match {
    case "d" => Some(Debit)
    case "c" => Some(Credit)
    case _ => None
  }




}


case class Balance(amount: Amount, debitCredit: TransactionType)



//case class AccountStatus(no: String,balance: Balance = Balance(0, Debit),no_withdrawals_today: Int,no_deposits_today: Int,deposits_today: Amount,withdrawals_today: Amount)



case class Transaction( accountNo: String, debitCredit: TransactionType, amount: Amount, date: DateTime = today)



