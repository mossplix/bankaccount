organization in ThisBuild := "moses.mugisha"
version in ThisBuild := "1.0-SNAPSHOT"

// the Scala version that will be used for cross-compiled libraries
scalaVersion in ThisBuild := "2.11.8"

val macwire = "com.softwaremill.macwire" %% "macros" % "2.2.5" % "provided"
val scalaTest = "org.scalatest" %% "scalatest" % "3.0.1" % Test
val playJsonDerivedCodecs = "org.julienrf" %% "play-json-derived-codecs" % "3.3"

lazy val `bankaccount` = (project in file("."))
  .aggregate(`bankaccount-api`, `bankaccount-impl`, `bankaccount-stream-api`, `bankaccount-stream-impl`)

lazy val `bankaccount-api` = (project in file("bankaccount-api"))
  .settings(
    libraryDependencies ++= Seq(
      lagomScaladslApi
    )
  )

lazy val `bankaccount-impl` = (project in file("bankaccount-impl"))
  .enablePlugins(LagomScala)
  .settings(
    libraryDependencies ++= Seq(
      lagomScaladslPersistenceCassandra,
      lagomScaladslKafkaBroker,
      lagomScaladslTestKit,
      macwire,
      scalaTest,
      playJsonDerivedCodecs
    )
  )
  .settings(lagomForkedTestSettings: _*)
  .dependsOn(`bankaccount-api`)

lazy val `bankaccount-stream-api` = (project in file("bankaccount-stream-api"))
  .settings(
    libraryDependencies ++= Seq(
      lagomScaladslApi
    )
  )

lazy val `bankaccount-stream-impl` = (project in file("bankaccount-stream-impl"))
  .enablePlugins(LagomScala)
  .settings(
    libraryDependencies ++= Seq(
      lagomScaladslTestKit,
      macwire,
      scalaTest,
      playJsonDerivedCodecs
    )
  )
  .dependsOn(`bankaccount-stream-api`, `bankaccount-api`)



