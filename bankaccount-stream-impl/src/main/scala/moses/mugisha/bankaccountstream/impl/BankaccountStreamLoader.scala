package moses.mugisha.bankaccountstream.impl

import com.lightbend.lagom.scaladsl.api.ServiceLocator.NoServiceLocator
import com.lightbend.lagom.scaladsl.server._
import com.lightbend.lagom.scaladsl.devmode.LagomDevModeComponents
import play.api.libs.ws.ahc.AhcWSComponents
import moses.mugisha.bankaccountstream.api.BankaccountStreamService
import moses.mugisha.bankaccount.api.BankaccountService
import com.softwaremill.macwire._

class BankaccountStreamLoader extends LagomApplicationLoader {

  override def load(context: LagomApplicationContext): LagomApplication =
    new BankaccountStreamApplication(context) {
      override def serviceLocator = NoServiceLocator
    }

  override def loadDevMode(context: LagomApplicationContext): LagomApplication =
    new BankaccountStreamApplication(context) with LagomDevModeComponents

  override def describeServices = List(
    readDescriptor[BankaccountStreamService]
  )
}

abstract class BankaccountStreamApplication(context: LagomApplicationContext)
  extends LagomApplication(context)
    with AhcWSComponents {

  // Bind the service that this server provides
  override lazy val lagomServer = serverFor[BankaccountStreamService](wire[BankaccountStreamServiceImpl])

  // Bind the BankaccountService client
  lazy val bankaccountService = serviceClient.implement[BankaccountService]
}
