package moses.mugisha.bankaccountstream.impl

import com.lightbend.lagom.scaladsl.api.ServiceCall
import moses.mugisha.bankaccountstream.api.BankaccountStreamService
import moses.mugisha.bankaccount.api.BankaccountService

import scala.concurrent.Future

/**
  * Implementation of the BankaccountStreamService.
  */
class BankaccountStreamServiceImpl(bankaccountService: BankaccountService) extends BankaccountStreamService {
  def stream = ServiceCall { balances =>
    Future.successful(balances.mapAsync(8)(bankaccountService.balance(_).invoke()))
  }
}
