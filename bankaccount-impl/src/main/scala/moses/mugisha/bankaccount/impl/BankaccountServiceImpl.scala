package moses.mugisha.bankaccount.impl

import akka.{Done, NotUsed}
import moses.mugisha.bankaccount.api
import moses.mugisha.bankaccount.api._
import com.lightbend.lagom.scaladsl.api.ServiceCall
import com.lightbend.lagom.scaladsl.api.broker.Topic
import com.lightbend.lagom.scaladsl.broker.TopicProducer
import com.lightbend.lagom.scaladsl.persistence.{EventStreamElement, PersistentEntityRegistry}
import java.util.UUID

import moses.mugisha.bankaccount.api.common._

import org.joda.time.DateTime

/**
  * Implementation of the BankaccountService.
  */
class BankaccountServiceImpl(persistentEntityRegistry: PersistentEntityRegistry) extends BankaccountService {



  override def balance(id: String) = ServiceCall[NotUsed, AccountStatus] { _ =>
    // Look up the Bankaccount entity for the given ID.
    val ref = persistentEntityRegistry.refFor[BankaccountEntity](id)

    // Ask the entity the Hello command.
    ref.ask(GetBalance(id))
  }

  override def debit(id: String) = ServiceCall[TransactionItem,Done] { transaction =>
    // Look up the Bankaccount entity for the given ID.
    val ref = persistentEntityRegistry.refFor[BankaccountEntity](id)

    // Tell the entity to use the greeting message specified.
    ref.ask(NewTransactionCommand(transaction))

  }

  override def credit(id: String) = ServiceCall[TransactionItem,Done] { transaction =>
    // Look up the Bankaccount entity for the given ID.
    val ref = persistentEntityRegistry.refFor[BankaccountEntity](id)

    // Tell the entity to use the greeting message specified.
    ref.ask(NewTransactionCommand(transaction))

  }


  override def transactionTopic(): Topic[api.TransactionItem] =
    TopicProducer.singleStreamWithOffset {
      fromOffset =>
        persistentEntityRegistry.eventStream(BankaccountEvent.Tag, fromOffset)
          .map(ev => (convertEvent(ev), ev.offset))
    }

  private def convertEvent(balanceEvent: EventStreamElement[BankaccountEvent]): api.TransactionItem = {
    balanceEvent.event match {
      case NewTransactionEvent(transaction) => api.TransactionItem(transaction.acc,transaction.amount,transaction.credit)

    }
  }
}
