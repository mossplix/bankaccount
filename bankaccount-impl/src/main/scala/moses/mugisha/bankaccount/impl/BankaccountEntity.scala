package moses.mugisha.bankaccount.impl

import java.time.LocalDateTime

import akka.Done
import com.lightbend.lagom.scaladsl.api.transport.{ExceptionMessage, PolicyViolation, TransportErrorCode}
import com.lightbend.lagom.scaladsl.persistence.{AggregateEvent, AggregateEventTag, PersistentEntity}
import com.lightbend.lagom.scaladsl.persistence.PersistentEntity.ReplyType
import com.lightbend.lagom.scaladsl.playjson.{JsonSerializer, JsonSerializerRegistry}
import org.joda.time.{DateTime, Instant}
import play.api.libs.json.{Format, Json}

import scala.collection.immutable.Seq
import com.lightbend.lagom.scaladsl.persistence.PersistentEntity.ReplyType
import moses.mugisha.bankaccount.api._
import com.lightbend.lagom.scaladsl.api.transport.{TransportErrorCode, TransportException}

object common {
  type Amount = BigDecimal
  type Error = String
  val today = DateTime.now()
  val MaxDailyDeposit = 150000
  val MaxDepositPerTransaction = 40000
  val MaxDailyDepositFrequency = 4
  val MaxDailyWithdrawal = 50000
  val MaxPerTransactionDeposit = 40000
  val MaxPerTransactionWithdrawal = 20000
  val MaxDailyWithdrawalFrequency = 3
}

import common._



class BankaccountEntity extends PersistentEntity {
  override type Command = BankaccountCommand[_]
  override type Event = BankaccountEvent
  override type State = BankaccountState

  /**
    * The initial state. This is used if there is no snapshotted state to be found.
    */
  override def initialState: BankaccountState = BankaccountState.newEntity

  /**
    * An entity can define different behaviours for different states, so the behaviour
    * is a function of the current state to a set of actions.
    */


  override def behavior: Behavior = {
    case BankaccountState(Some(AccountStatus("",_,_,_,_,_,_)), _) => newAccount
    case BankaccountState(Some(accountStatus), _) => existingAccount
  }


  def newAccount: Actions = {
    Actions()


      .onReadOnlyCommand[GetBalance, AccountStatus] {

      // Command handler for the Hello command
      case (GetBalance(acc), ctx, state) =>
        // Reply with a message built from the current message, and the name of
        // the person we're meant to say balance to.
        ctx.reply(state.accountStatus.get)

    }.onCommand[NewTransactionCommand, Done] {

      case (NewTransactionCommand(transaction), ctx, state) if transaction.amount > state.accountStatus.get.balance && transaction.credit == true =>
        throw BadRequest("Insufficient Funds On your account!")

      case (NewTransactionCommand(transaction),ctx,state) if transaction.amount > MaxPerTransactionWithdrawal && transaction.credit == false =>
        throw BadRequest(s"Your Transaction of ${transaction.amount} is way above the transaction limit of MaxPerTransactionWithdrawal")

      case ((NewTransactionCommand(transaction),ctx,state)) if state.getDailyWithdrawalsCount()== MaxDailyWithdrawalFrequency && transaction.credit == false=>
         throw BadRequest(s"You have exceeded your number of allowed daily withdrawals")

      case (NewTransactionCommand(transaction), ctx, state) if state.getDailyDepositsCount() == MaxDailyDepositFrequency && transaction.credit == true=>
        throw BadRequest("You have exceeded your number of allowed daily deposits transactions")

      case (NewTransactionCommand(transaction), ctx, state) if transaction.amount > MaxPerTransactionDeposit && transaction.credit == false =>
        throw BadRequest(s"Your Transaction of ${transaction.amount} is way above the transaction limit of MaxPerTransactionDeposit")

      case ((NewTransactionCommand(transaction), ctx, state)) if state.getDailyWithdrawalSum()+transaction.amount> MaxDailyWithdrawal && transaction.credit == true =>
        throw BadRequest(s"Your can only withdraw up to ${MaxDailyDeposit}")

      case (NewTransactionCommand(transaction), ctx, state) if state.getDailyDepositsSum()+transaction.amount < MaxDailyDeposit && transaction.credit == false =>
        throw BadRequest(s"Your can only withdraw up to ${MaxDailyDeposit}")


      case (NewTransactionCommand(transaction), ctx, state) =>
        // In response to this command, we want to first persist it as a
        // GreetingMessageChanged event
        ctx.thenPersist(
          NewTransactionEvent(transaction)
        ) { _ =>
          // Then once the event is successfully persisted, we respond with done.
          ctx.reply(Done)
        }
    }
      .onEvent {
        case (NewTransactionEvent(transaction), state) =>



          state.copy(accountStatus = Some(AccountStatus(transaction.acc)))


          state.updateState(transaction)


      case (GetBalanceEvent(acc), state) =>
              state.copy(accountStatus = Some(AccountStatus(acc)))


     }
  }


  def existingAccount: Actions = {
    Actions()
      .onReadOnlyCommand[GetBalance, AccountStatus] {

      case (GetBalance(acc), ctx, state) =>
        // Reply with a message built from the current message, and the name of
        // the person we're meant to say balance to.
        ctx.reply(state.accountStatus.get)
    }
      .onCommand[NewTransactionCommand, Done] {

      case (NewTransactionCommand(transaction), ctx, state) if transaction.amount > state.accountStatus.get.balance && transaction.credit == true =>
        throw BadRequest("Insufficient Funds On your account!")

      case (NewTransactionCommand(transaction),ctx,state) if transaction.amount > MaxPerTransactionWithdrawal && transaction.credit == false =>
        throw BadRequest(s"Your Transaction of ${transaction.amount} is way above the transaction limit of MaxPerTransactionWithdrawal")

      case ((NewTransactionCommand(transaction),ctx,state)) if state.getDailyWithdrawalsCount()== MaxDailyWithdrawalFrequency && transaction.credit == false=>
        throw BadRequest(s"You have exceeded your number of allowed daily withdrawals")

      case (NewTransactionCommand(transaction), ctx, state) if state.getDailyDepositsCount() == MaxDailyDepositFrequency && transaction.credit == true=>
        throw BadRequest("You have exceeded your number of allowed daily deposits transactions")

      case (NewTransactionCommand(transaction), ctx, state) if transaction.amount > MaxPerTransactionDeposit && transaction.credit == false =>
        throw BadRequest(s"Your Transaction of ${transaction.amount} is way above the transaction limit of MaxPerTransactionDeposit")

      case ((NewTransactionCommand(transaction), ctx, state)) if state.getDailyWithdrawalSum()+transaction.amount> MaxDailyWithdrawal && transaction.credit == true =>
        throw BadRequest(s"Your can only withdraw up to ${MaxDailyDeposit}")

      case (NewTransactionCommand(transaction), ctx, state) if state.getDailyDepositsSum()+transaction.amount < MaxDailyDeposit && transaction.credit == false =>
        throw BadRequest(s"Your can only withdraw up to ${MaxDailyDeposit}")
        case (NewTransactionCommand(transaction), ctx, state) =>
          // In response to this command, we want to first persist it as a
          // GreetingMessageChanged event
          ctx.thenPersist(
            NewTransactionEvent(transaction)
          ) { _ =>
            // Then once the event is successfully persisted, we respond with done.
            ctx.reply(Done)
          }
    }
      .onEvent {
        case (NewTransactionEvent(transaction), state) =>


          state.updateState(transaction)


  }
}}

case class BankaccountState(accountStatus: Option[AccountStatus], dailyTransactions: Seq[TransactionItem]) {

  def updateState(transaction: TransactionItem): BankaccountState = {
    val accState = transaction.credit

    accState match {


      case (true) => copy(accountStatus = Some(accountStatus.get.copy(balance = accountStatus.get.balance + transaction.amount)), dailyTransactions = dailyTransactions :+ transaction)
      case (false) => copy(accountStatus = Some(accountStatus.get.copy(balance = accountStatus.get.balance - transaction.amount)), dailyTransactions = dailyTransactions :+ transaction)

    }

  }

  def getDailyWithdrawalsCount():BigDecimal =
  {
    dailyTransactions.filter(item => item.date == today.toLocalDate().toDate().toString&&item.credit == false).length
  }


  def getDailyDepositsCount():BigDecimal =
  {
    dailyTransactions.filter(item => item.date == today.toLocalDate().toDate().toString&&item.credit == true).length
  }


  def getDailyWithdrawalSum(): BigDecimal = {
    dailyTransactions.filter(item => item.date == today.toLocalDate().toDate().toString&&item.credit == false).map(_.amount).sum
  }

  def getDailyDepositsSum(): BigDecimal = {
    dailyTransactions.filter(item => item.date == today.toLocalDate().toDate().toString&&item.credit == false).map(_.amount).sum
  }




}





object BankaccountState {

  val newEntity = BankaccountState(Some(AccountStatus("")), Seq.empty[TransactionItem])
  implicit val format: Format[BankaccountState] = Json.format




}

sealed trait BankaccountEvent extends AggregateEvent[BankaccountEvent] {
  def aggregateTag = BankaccountEvent.Tag
}

object BankaccountEvent {
  val Tag = AggregateEventTag[BankaccountEvent]
}


case class NewTransactionEvent(transaction: TransactionItem) extends BankaccountEvent

object NewTransactionEvent {

  implicit val format: Format[NewTransactionEvent] = Json.format
}


case class GetBalanceEvent(name:String) extends BankaccountEvent

object GetBalanceEvent {


  implicit val format: Format[GetBalanceEvent] = Json.format
}


sealed trait BankaccountCommand[R] extends ReplyType[R]



case class GetBalance(name: String) extends BankaccountCommand[AccountStatus]

object GetBalance{
  implicit val format: Format[GetBalance] = Json.format
}



case class NewTransactionCommand(transaction: TransactionItem) extends BankaccountCommand[Done]

object NewTransactionCommand{
  implicit val format: Format[NewTransactionCommand] = Json.format
}



object BankaccountSerializerRegistry extends JsonSerializerRegistry {
  override def serializers: Seq[JsonSerializer[_]] = Seq(
    JsonSerializer[GetBalance],
    JsonSerializer[NewTransactionEvent],
    JsonSerializer[NewTransactionCommand],
    JsonSerializer[BankaccountState],
      JsonSerializer[AccountStatus]
  )
}


final class BadRequest(errorCode: TransportErrorCode, exceptionMessage: ExceptionMessage, cause: Throwable) extends TransportException(errorCode, exceptionMessage, cause) {
  def this(errorCode: TransportErrorCode, exceptionMessage: ExceptionMessage) = this(errorCode, exceptionMessage, null)
}
object BadRequest {
  val ErrorCode = TransportErrorCode.BadRequest

  def apply(message: String) = new BadRequest(
    ErrorCode,
    new ExceptionMessage(classOf[BadRequest].getSimpleName, message), null
  )

  def apply(cause: Throwable) = new PolicyViolation(
    ErrorCode,
    new ExceptionMessage(classOf[BadRequest].getSimpleName, cause.getMessage), cause
  )
}
