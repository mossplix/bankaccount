package moses.mugisha.bankaccount.impl

import akka.actor.ActorSystem
import akka.testkit.TestKit
import com.lightbend.lagom.scaladsl.testkit.PersistentEntityTestDriver
import com.lightbend.lagom.scaladsl.playjson.JsonSerializerRegistry
import moses.mugisha.bankaccount.api.TransactionItem
import org.joda.time.DateTime
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpec}

class BankaccountEntitySpec extends WordSpec with Matchers with BeforeAndAfterAll {

  private val system = ActorSystem("BankaccountEntitySpec",
    JsonSerializerRegistry.actorSystemSetupFor(BankaccountSerializerRegistry))

  override protected def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }

  private def withTestDriver(block: PersistentEntityTestDriver[BankaccountCommand[_], BankaccountEvent, BankaccountState] => Unit): Unit = {
    val driver = new PersistentEntityTestDriver(system, new BankaccountEntity, "bankaccount-1")
    block(driver)
    driver.getAllIssues should have size 0
  }

  "BankAccount entity" should {

    "get balance by default" in withTestDriver { driver =>
      val outcome = driver.run(GetBalance("acc-1"))
      outcome.replies should contain only "0"
    }

    "attemp to withdrawal with 0 balance" in withTestDriver { driver =>
      val tr = TransactionItem(acc="acc",amount=500,credit=true,DateTime.now())
      val outcome1 = driver.run(NewTransactionCommand(tr))
      outcome1.events should contain only tr
      val outcome2 = driver.run(NewTransactionCommand(tr))
      outcome2.replies should contain only ""
    }


    "attemp to withdrawal with more than the maximum  allowed" in withTestDriver { driver =>
      val tr = TransactionItem(acc="acc",amount=500,credit=true)
      val outcome1 = driver.run(NewTransactionCommand(tr))
      outcome1.events should contain only tr
      val outcome2 = driver.run(NewTransactionCommand(tr))
      outcome2.replies should contain only ""
    }

    "attemp to deposit with more than the maximum  allowed" in withTestDriver { driver =>
      val tr = TransactionItem(acc="acc",amount=500,credit=true)
      val outcome1 = driver.run(NewTransactionCommand(tr))
      outcome1.events should contain only tr
      val outcome2 = driver.run(NewTransactionCommand(tr))
      outcome2.replies should contain only ""
    }

    "attemp to carry out more deposit transactions than allowed" in withTestDriver { driver =>
      val tr = TransactionItem(acc="acc",amount=500,credit=true)
      val outcome1 = driver.run(NewTransactionCommand(tr))
      outcome1.events should contain only tr
      val outcome2 = driver.run(NewTransactionCommand(tr))
      outcome2.replies should contain only ""
    }

    "attemp to carry out more withdrawal transactions than allowed" in withTestDriver { driver =>
      val tr = TransactionItem(acc="acc",amount=500,credit=true)
      val outcome1 = driver.run(NewTransactionCommand(tr))
      outcome1.events should contain only tr
      val outcome2 = driver.run(NewTransactionCommand(tr))
      outcome2.replies should contain only ""
    }

    "attemp to carry out more withdrawal more than the accepted maxim in small sessions" in withTestDriver { driver =>
      val tr = TransactionItem(acc="acc",amount=500,credit=true)
      val outcome1 = driver.run(NewTransactionCommand(tr))
      outcome1.events should contain only tr
      val outcome2 = driver.run(NewTransactionCommand(tr))
      outcome2.replies should contain only ""
    }

    "attemp to carry out more deposit more than the accepted maximum in small sessions" in withTestDriver { driver =>
      val tr = TransactionItem(acc="acc",amount=500,credit=true)
      val outcome1 = driver.run(NewTransactionCommand(tr))
      outcome1.events should contain only tr
      val outcome2 = driver.run(NewTransactionCommand(tr))
      outcome2.replies should contain only ""
    }

  }
}
