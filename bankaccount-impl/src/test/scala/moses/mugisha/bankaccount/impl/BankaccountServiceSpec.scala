package moses.mugisha.bankaccount.impl

import com.lightbend.lagom.scaladsl.server.LocalServiceLocator
import com.lightbend.lagom.scaladsl.testkit.ServiceTest
import org.scalatest.{AsyncWordSpec, BeforeAndAfterAll, Matchers}
import moses.mugisha.bankaccount.api._

class BankaccountServiceSpec extends AsyncWordSpec with Matchers with BeforeAndAfterAll {

  private val server = ServiceTest.startServer(
    ServiceTest.defaultSetup
      .withCassandra(true)
  ) { ctx =>
    new BankaccountApplication(ctx) with LocalServiceLocator
  }

  val client = server.serviceClient.implement[BankaccountService]

  override protected def afterAll() = server.stop()

  "BankAccount service" should {

    "get balance" in {
      client.balance("acc-1").invoke().map { answer =>
        answer should ===("0")
      }
    }

    "" in {
      for {
        _ <- client.debit("Bob").invoke(Transaction("Hi"))
        answer <- client.balance("Bob").invoke()
      } yield {
        answer should ===("Hi, Bob!")
      }
    }
  }
}
