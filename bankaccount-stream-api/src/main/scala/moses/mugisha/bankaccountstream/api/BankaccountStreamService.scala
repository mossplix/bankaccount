package moses.mugisha.bankaccountstream.api

import akka.NotUsed
import akka.stream.scaladsl.Source
import com.lightbend.lagom.scaladsl.api.{Service, ServiceCall}

/**
  * The BankAccount stream interface.
  *
  * This describes everything that Lagom needs to know about how to serve and
  * consume the BankaccountStream service.
  */
trait BankaccountStreamService extends Service {

  def stream: ServiceCall[Source[String, NotUsed], Source[String, NotUsed]]

  override final def descriptor = {
    import Service._

    named("bankaccount-stream")
      .withCalls(
        namedCall("stream", stream)
      ).withAutoAcl(true)
  }
}

